﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace SlimeGame
{
    /// <summary>
    /// the special pickups that the player is tasked to fetch
    /// </summary>
    class Organ : GameObject
    {
        OrganType _organ;
        float i;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        /// <param name="spriteName"></param>
        /// <param name="pos"></param>
        /// <param name="worldScale"></param>
        /// <param name="organ"></param> type of organ
        public Organ(ContentManager content, string spriteName, Vector2 pos, int worldScale, OrganType organ) : base(content, spriteName, pos, worldScale)
        {
            _organ = organ;

        }

        public override void Update(GameTime gametime)
        {
            base.Update(gametime);
            if (IsColliding(GameWorld.Slime))
            {
                if (_organ == OrganType.brain)
                    GameWorld.FoundBrain = true;
                else if (_organ == OrganType.eye)
                    GameWorld.FoundEye = true;
                else if (_organ == OrganType.heart)
                    GameWorld.FoundHeart = true;

                GameWorld.RemoveGameObject(this, 1);
                //GameWorld.UpdateUi();
            }
            i+=.05f;
            _position.Y += (float)Math.Sin((double)i);
        }
    }
}

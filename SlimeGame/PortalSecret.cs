﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace SlimeGame
{
    /// <summary>
    /// class to manage secret portals, that teleports the player to a location in the same level
    /// </summary>
    class PortalSecret : AnimatedGameObject
    {
        private Vector2 _secretExit;
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="frameCount"></param>
        /// <param name="animFPS"></param>
        /// <param name="content"></param>
        /// <param name="spriteName"></param>
        /// <param name="pos"></param>
        /// <param name="worldScale"></param>
        /// <param name="secretExit"></param> portal exit
        /// <param name="secret"></param> 
        public PortalSecret(int frameCount, float animFPS, ContentManager content, string spriteName, Vector2 pos, int worldScale, Vector2 secretExit) : base(frameCount, animFPS, content, spriteName, pos, worldScale)
        {
            _secretExit = secretExit;
        }


        public override Rectangle CollisionBox
        {
            get
            {
                int w = _animationRectangles[0].Width * _scaleFactor;
                return new Rectangle((int)_position.X + (int)(w * 0.2f), (int)_position.Y, w / 2, (int)((_animationRectangles[0].Height * _scaleFactor) *.9f));
            }
        }


        public override void Update(GameTime gametime)
         {
            base.Update(gametime);
            if (IsColliding(GameWorld.Slime))
            {
                    GameWorld.Slime.SetSecretExit(_secretExit);
            }
        }
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlimeGame
{
    /// <summary>
    /// GameObject is a container for game objects
    /// </summary>
    public class GameObject
    {
        protected Texture2D _sprite;
        protected Vector2 _position;
        protected int _scaleFactor;
        protected Vector2 _scaleVector;

        /// <summary>
        /// base constructor for game objects
        /// </summary>
        /// <param name="content">the content manager</param>
        /// <param name="spriteName">name of texture in contentmanager</param>
        /// <param name="pos">start position</param>
        public GameObject(ContentManager content, string spriteName, Vector2 pos, int worldScale)
        {
            _sprite = content.Load<Texture2D>(spriteName);
            _position = pos;
            _scaleFactor = worldScale;
            _scaleVector = new Vector2((float)worldScale, (float)worldScale);
        }


        /// <summary>
        /// returns a rectangle with the size of gameobjects sprite
        /// </summary>
        public virtual Rectangle CollisionBox
        {
            get
            {
                return new Rectangle((int)_position.X, (int)_position.Y, _sprite.Width * _scaleFactor, _sprite.Height * _scaleFactor);
            }
        }

       
       

        public bool IsColliding(GameObject otherObj)
        {
            return CollisionBox.Intersects(otherObj.CollisionBox);
        }

       


        public virtual void DoCollision(GameObject otherObj)
        {
        }

        public virtual void Update(GameTime gametime)
        {
     

        }

        /// <summary>
        /// sends render information to spritebatch
        /// </summary>
        /// <param name="spriteBatch">the spritebatch whom handles rendering of gameobject </param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
           // spriteBatch.Draw(_sprite, _position, Color.White);
            spriteBatch.Draw(_sprite, _position - GameWorld.Camera, _sprite.Bounds, Color.White, 0f, new Vector2(0, 0), _scaleVector, SpriteEffects.None, 0f);


        }

        /// <summary>
        /// debug method to show a collision box at the gamesprites bounds
        /// </summary>
        /// <param name="spriteBatch">the spritebatch whom handles rendering of collision box</param>
        /// <param name="collisionTex">provided collision texture</param>
        public virtual void DrawCollisionBox(SpriteBatch spriteBatch, Texture2D collisionTex)
        {

            Rectangle cBox = CollisionBox;
            Rectangle top = new Rectangle(cBox.X, cBox.Y, cBox.Width, 1);
            Rectangle bottom = new Rectangle(cBox.X, cBox.Y + cBox.Height, cBox.Width, 1);
            Rectangle right = new Rectangle(cBox.X + cBox.Width, cBox.Y, 1, cBox.Height);
            Rectangle left = new Rectangle(cBox.X, cBox.Y, 1, cBox.Height);

            spriteBatch.Draw(collisionTex, top, Color.Red);
            spriteBatch.Draw(collisionTex, bottom, Color.Red);
            spriteBatch.Draw(collisionTex, right, Color.Red);
            spriteBatch.Draw(collisionTex, left, Color.Red);

        }
    }
}

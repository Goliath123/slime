﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlimeGame
{
    /// <summary>
    /// enum to define 3 special pickups
    /// </summary>
    public enum OrganType : int
    {
        eye = 1,
        brain=2,
        heart=3
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace SlimeGame
{
    /// <summary>
    /// climbable stair
    /// </summary>
    class Stair : Brick
    {
        public Stair(int id, int worldScale, int frameCount, ContentManager content, string spriteName, Vector2 pos) : base(id, worldScale, frameCount, content, spriteName, pos)
        {
        }

        public Stair(int indexX, int indexY, int worldScale, int frameCount, ContentManager content, string spriteName, Vector2 pos) : base(indexX, indexY, worldScale, frameCount, content, spriteName, pos)
        {
            _scaleVector = new Vector2((float)_scaleFactor / 4, (float)_scaleFactor / 4);
            _typeIndexX = indexX;
            _typeIndexY = indexY;
            _rect = new Rectangle(_typeIndexX * _sprite.Width / frameCount, _typeIndexY * _sprite.Width / frameCount, _sprite.Width / frameCount, _sprite.Height / frameCount);
        }


        public override Rectangle CollisionBox
        {

            get
            {
                int w = _rect.Width * _scaleFactor / 4;
                return new Rectangle((int)_position.X + (int)(w * 0.4f), (int)_position.Y, w / 6, _rect.Height * _scaleFactor / 4);
            }
        }
       

    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlimeGame
{
    /// <summary>
    /// static class to manage levels
    /// </summary>
    public static class Worlds
    {
        private static int _id = 0;
        private static Brick _arrowBrick;

        private static int _typeIndexX = 0;
        private static int _typeIndexY = 0;
        private static int _worldIndex = -1;

        private static List<WorldInfo> _worlds = new List<WorldInfo>();

        public static bool initialized = false;


        /// <summary>
        /// constructs all world structs into _worlds list
        /// </summary>
        public static void Initialize()
        {
            _worlds.Add(new WorldInfo(new int[15, 27]
         {
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0,-1, 0, 0, 0, 0, 0, 0,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,90, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,88,88,88,15, 0, 0, 0, 0, 0,88,88,89,88,88},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,-7,88,88,88, 0, 0, 0, 0, 0,88,88,88, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88, 0, 0},
         {88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88, 0, 0, 0, 0, 0, 0},
         {88,88, 0, 0, 0, 0, 0,88,88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,15, 0, 0, 0},
         {88,88,-6, 0, 0, 0,88,88,88,88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88, 0, 0},
         {88,88,88,88,88,88,88,88,88,88,88,88,88,88,88,88,88,88,88,88,88, 0, 0, 0, 0, 0, 0},
         { 0,88,88,88,88,88,88,88, 0, 0, 0,88,88,88,88,88,88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0}
         },
          new Vector2(0, 0),        //worldoffset
          new Vector2(0, 0),        //cam
          4,                         //scale
          "sky",
          "Slime intuitively knew to use the arrow keys to move"

          ));


            _worlds.Add(new WorldInfo(
       new int[14, 45]
         {
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88},
         { 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,15, 0, 0, 0, 0, 0, 0, 0,90, 0,88},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,88,88, 0, 0, 0, 0, 0, 0,89, 0,88},
         {15,15,15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,88,88},
         {88,88,88, 0, 0, 0, 0, 0, 0, 0, 0,93,88,88,88,88,88,88,12,88,88,88,88,88,88,88,88,88,88,88,88,88,88,88,88,88,88,88,88,88,88,88,88,88,88},
         { 0,88,88, 0, 0, 0, 0, 0, 0, 0, 0,89,88, 0, 0, 0, 0,15,11,88,88, 0,88,88,88,88, 0,88, 0,88,88,88,88,88,88,88,88,88,88,88,-7,88,88,88, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88, 0, 0, 0, 0, 0,88,88,88, 0,88, 0, 0, 0, 0, 0, 0, 0, 0,88,88, 0, 0, 0, 0, 0, 0,88,88,88,88, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
         },
            new Vector2(-80, 400),      //worldoffset
            new Vector2(110, 00),        //cam
            2,                           //scale
            "sky"
            
        ));

            _worlds.Add(new WorldInfo(
                 new int[5, 12]
              {
         {87, 0, 0, 0, 0, 0, 0,87, 0, 0, 0, 0 },
         {87, 0,-1, 0, 0, 0, 0,87, 0, 0, 0, 0 },
         {87, 0, 0,-9,-8, 0, 0,87, 0, 0, 0, 0 },
         {87, 0,-6, 0, 0, 0,75,87, 0, 0, 0, 0 },
         {87,87,87,87,87,87,87,87, 0, 0, 0, 0 },
              },
                 new Vector2(0, 0),    //worldoffset
                 new Vector2(0, 0),        //cam
                 10,                          //scale
                 "dark",
                 "Hi Slime, fetch me an eye,\n brain and a heart \nfor a smashing HALLOWEEN PUNCH"
             ));

            _worlds.Add(new WorldInfo(
  new int[17, 45]
    {
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88, 0, 0,18, 0, 0, 0, 0},
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,15, 0, 0, 0, 0, 0, 0,88, 0, 0, 0, 0, 0, 0, 0},
        { 0,-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88, 0, 0, 0, 0},
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,12,88,88,88,88,88,88, 0, 0, 0,88,88,88, 0, 0, 0},
        {88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11,88,-7, 0, 0,88, 0, 0,88, 0, 0, 0, 0, 0, 0, 0},
        {88,88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {88,88,88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {88,88,88,88,88, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,90, 0, 0, 0, 0},
        {88, 0,88,88,88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,12,88,88,88, 0,88,88,88,88,11, 0, 0, 0, 0, 0, 0, 0, 0,88,88,89,88,88, 0, 0},
        {88,92, 0,  0, 0, 0, 0, 0, 0, 0,88,88,88,12,88, 0, 0, 0, 0, 0,11, 0, 0, 0, 0,88,88,88, 0,11, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,88, 0, 0, 0},
        {88,89, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88, 0, 0, 0, 0},
        {88,88,88,88,88,88,88,88, 0, 0, 0, 0, 0,11,15, 0, 0, 0,88,88,88, 0, 0, 0, 0, 0, 0, 0,88,88,88,88,12,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0, 2, 0, 0,88,88,88, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,88,88,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,15,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {15,15,15,93, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {88,88,88,89,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88, 0, 0, 0, 0, 0, 0, 0}

    },
       new Vector2(-200, 400),      //worldoffset
       new Vector2(-80, 100),        //cam
       3,                           //scale
       "sky"

   ));

            _worlds.Add(new WorldInfo(
             new int[17, 45]
               {
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0, 0, 0, 0,-7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0, 0, 0, 0,88,88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0,88,88, 0,88,15,15,88,88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0,88, 0,88, 0, 0, 0, 0,88,88, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {88,88,88, 0, 0,88,88, 0, 0,88, 0, 0, 0,88,88,88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {88,88, 0, 0,88,88,88,88, 0, 0,88, 0, 0, 0, 0,88,88,88,88, 0, 0,88,12,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {88, 0, 0,88, 0, 0, 0,88,88, 0, 0,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {88, 0,88,88, 0,19, 0,88,88,88, 0,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {88, 0, 0,88,92, 0, 0,88,88, 0, 0,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {88,88, 0, 0,88, 0, 0, 0, 0, 0,88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0,88,88, 0, 0,88,88, 0, 0,88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0,88,88,88, 0, 0, 0,88,88,88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0,88,88, 0,88,88, 0,88,88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0, 0, 0, 0, 0, 0,-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,88, 0, 0, 0, 0, 0, 0,90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0, 0, 0, 0, 0,88,88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,15,88,88,88, 0,88,89,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,88, 0, 0, 0, 0,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

               },
                  new Vector2(0, 0),      //worldoffset
                  new Vector2(-80, 100),        //cam
                  5,                           //scale
                  "sky"

              ));

            _worlds.Add(new WorldInfo(new int[45, 27]
       {
         { 0, 0, 0, 0, 0, 0, 0,17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0,90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         {88,89,88, 0, 0, 0, 0,88, 0, 0, 0,88,88,88,88,12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0,88, 0, 0, 0, 0, 0,88, 0, 0, 0, 0,88,88, 0,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0,88, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,88,12,88,88, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,88,11, 0,88, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0,88, 0, 0, 0, 0, 0, 0},
         { 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0,15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0},
         {88,88, 0, 0, 0, 0, 0, 0, 0, 0,92,88,12,88,88, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0,88, 0, 0, 0, 0, 0, 0, 0, 0,88,88,11,88,88, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0,88,88,12,88,88, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0,11, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0,15, 0, 0, 0,88,88,88,88,88,88, 0, 0,88,88, 0,88, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0,88,88, 0, 0,88,-7,88,88, 0, 0, 0,88,88,88, 0,88,88,88, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0,88, 0, 0,88,88,88, 0, 0, 0, 0,88,88,88, 0,88, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0,88, 0, 0, 0,88,88, 0, 0, 0,88,88, 0,88, 0,88, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88, 0, 0, 0,88, 0, 0, 0, 0,88, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88, 0,88,88,88, 0, 0,88,88, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88, 0, 0, 0,88, 0, 0, 0,88, 0,88, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88, 0, 0, 0,88,88, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,88,12,88,88, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,88,88,88, 0, 0, 0,12,88, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,88,15, 0, 0, 0, 0,11,88, 0,88, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0,88, 0,88, 0, 0, 0, 0, 0,11,88, 0,88, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88, 0, 0, 0,88,88,88, 0,88, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,88, 0, 0,88,88,88, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0,88, 0, 0,88, 0, 0, 0,88, 0,88, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0,88, 0, 0,88, 0, 0, 0,88, 0,88, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,12,88,88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,11, 0,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,11, 0,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,-1, 0,11, 0,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,11, 0,88, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,88,88,88,88,88,88,88,88,88, 0, 0, 0, 0, 0, 0},
         { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,88,88,88,88,88,88,88, 0, 0, 0, 0, 0, 0, 0, 0, 0}
       },
        new Vector2(0, 0),        //worldoffset
        new Vector2(0, 2000),        //cam
        4,                         //scale
        "sky"

        ));

            _worlds.Add(new WorldInfo(
                new int[6, 12]
             {
         {87, 0, 0, 0, 0, 0,-8,87, 0, 0, 0, 0 },
         {87, 0,-1, 0, 0, 0, 0,87, 0, 0, 0, 0 },
         {87, 0, 0,-9, 0, 0, 0,87, 0, 0, 0, 0 },
         {87, 0,-6, 0, 0, 0, 0,87, 0, 0, 0, 0 },
         {87,87,87,87,87,87,87,87, 0, 0, 0, 0 },
         {87, 0, 0,-7, 0, 0, 0,87, 0, 0, 0, 0 }

             },
                new Vector2(0, 0),    //worldoffset
                new Vector2(0, -200),        //cam
                8,                          //scale
                "dark",
                "Slime, lets have some\n HALLOWEEN PUNCH"
            ));

        }


        /// <summary>
        /// returns the next world index 
        /// </summary>
        /// <returns></returns>
        /// 
        public static int NextLevelIndex()
        {
            if (_worldIndex + 1 < _worlds.Count)
                return _worldIndex + 1;
            return 0;
        }

        public static int LevelIndex()
        {
                return _worldIndex ;
        }

        public static int NextLevelScale(int nextLevel)
        {
            return _worlds[nextLevel].worldScale;
        }

        public static string NextLevelBg()
        {
            if (_worldIndex + 1 < _worlds.Count)
                return _worlds[_worldIndex + 1].background;
            return "sky";
        }

        public static Vector2 LevelCamOrigin()
        {
            Console.WriteLine(_worlds[_worldIndex].camOrigin + "cams");
            return _worlds[_worldIndex].camOrigin;
        }

        private static string DisplayString()
        {
            return _worlds[_worldIndex].displayString;
        }

        public static float LevelScale()
        {
            if (_worldIndex >= 0f)
                return (float)_worlds[_worldIndex].worldScale;
            return 0f;
        }

        /// <summary>
        /// looks in _worlds[worldIndex].world for -1 and returns a spawnpoint vector
        /// </summary>
        /// <param name="worldScale"></param>
        /// <param name="worldIndex"></param>
        /// <returns></returns>
        public static Vector2 GetSpawnPoint(int worldScale, int worldIndex) 
        {
            for (int x = 0; x < _worlds[worldIndex].world.GetLength(1); x++)
            {
                for (int y = 0; y < _worlds[worldIndex].world.GetLength(0); y++)
                {
                    if (_worlds[worldIndex].world[y, x] == -1)            //spawnpoint
                        return new Vector2(x * 16 * worldScale, y * 16 * worldScale) + WorldOrigin(worldIndex);
                }
            }
            return new Vector2(1 * 16 * worldScale, 1 * 16 * worldScale);
        }

        public static Vector2 GetCauldronPos(int worldScale, int worldIndex) 
        {
            for (int x = 0; x < _worlds[worldIndex].world.GetLength(1); x++)
            {
                for (int y = 0; y < _worlds[worldIndex].world.GetLength(0); y++)
                {
                    if (_worlds[worldIndex].world[y, x] == -9)            
                        return new Vector2(x * 16 * worldScale, y * 16 * worldScale) + WorldOrigin(worldIndex);
                }
            }
            return new Vector2(1 * 16 * worldScale, 1 * 16 * worldScale);
        }

        public static Vector2 GetSecretExit(int worldScale, int exit) //TODO: consider merge with GetSpawnpoint method
        {
            for (int x = 0; x < _worlds[_worldIndex].world.GetLength(1); x++)
            {
                for (int y = 0; y < _worlds[_worldIndex].world.GetLength(0); y++)
                {
                    if (_worlds[_worldIndex].world[y, x] == exit)
                        return new Vector2(x * 16 * worldScale, y * 16 * worldScale) + WorldOrigin(_worldIndex);
                }
            }
            return new Vector2(1 * 16 * worldScale, 1 * 16 * worldScale);
        }

        private static Vector2 WorldOrigin(int worldIndex)
        {
            //if (_worldOrigin.GetLength(0) < worldIndex)
            return _worlds[worldIndex].worldOrigin;
            return new Vector2(0, 0);
        }

        public static bool IsShowingLitArrow { get; set; } = false;
        public static void ShowLitArrow()
        {
            _arrowBrick.ChangeTexture(7, 1);
        }
        public static void ShowUnlitArrow()
        {
            _arrowBrick.ChangeTexture(7, 2);
        }

        /// <summary>
        /// builds the world by iterating through _worlds[worldIndex].world, and adding the gameobject that the number in the 2d array represents
        /// </summary>
        /// <param name="content"></param>
        /// <param name="worldIndex"></param>
        /// <param name="worldScale"></param>
        public static void BuildWorld(ContentManager content, int worldIndex, int worldScale)
        {
            if (!initialized)
            {
                Initialize();
                initialized = true;
            }

            GameWorld.ShowScore = false;
            _worldIndex = worldIndex;
            for (int x = 0; x < _worlds[_worldIndex].world.GetLength(1); x++)
            {
                for (int y = 0; y < _worlds[_worldIndex].world.GetLength(0); y++)
                {
                    if (_worlds[_worldIndex].world[y, x] == 88)                                         //brick
                    {
                        Vector2 pos = new Vector2(x * 16 * worldScale, y * 16 * worldScale) + WorldOrigin(worldIndex);
                        GameWorld.AddGameObject(new Brick(_id, worldScale, 8, content, "bricks2", pos), 2);
                        AddGrass(x, y, worldScale, content, pos);
                    }
                    else if (_worlds[_worldIndex].world[y, x] == 87)           //transparent brick
                    {
                        Vector2 pos = new Vector2(x * 16 * worldScale, y * 16 * worldScale) + WorldOrigin(worldIndex);
                        GameWorld.AddGameObject(new Brick(7, 0, worldScale, 8, content, "bricks2", pos), 2);
                    }
                    else if (_worlds[_worldIndex].world[y, x] == 89)          //arrow brick
                    {
                        Vector2 pos = new Vector2(x * 16 * worldScale, y * 16 * worldScale) + WorldOrigin(worldIndex);
                        _arrowBrick = new Brick(7, 2, worldScale, 8, content, "bricks2", pos);
                        GameWorld.AddGameObject(_arrowBrick, 2);
                        AddGrass(x, y, worldScale, content, pos);

                    }
                    else if (_worlds[_worldIndex].world[y, x] == 11)          //stair 
                    {
                        Vector2 pos = new Vector2(x * 16 * worldScale, y * 16 * worldScale) + WorldOrigin(worldIndex);
                        GameWorld.AddGameObject(new Stair(7, 4, worldScale, 8, content, "bricks2", pos), 1);
                    }
                    else if (_worlds[_worldIndex].world[y, x] == 12)          //stair BRICK
                    {
                        Vector2 pos = new Vector2(x * 16 * worldScale, y * 16 * worldScale) + WorldOrigin(worldIndex);
                        AddGrass(x, y, worldScale, content, pos);
                        GameWorld.AddGameObject(new Stair(7, 3, worldScale, 8, content, "bricks2", pos), 1);

                    }
                    else if (_worlds[_worldIndex].world[y, x] == 15)          //gem

                    {
                        Vector2 pos = new Vector2(x * 16 * worldScale, y * 16 * worldScale) + WorldOrigin(worldIndex);
                        GameWorld.AddGameObject(new Gem(4, 8, content, "Gem", pos, worldScale), 1);
                    }
                    else if (_worlds[_worldIndex].world[y, x] == 90)          //portal

                    {
                        Vector2 pos = new Vector2(x * 16 * worldScale, (float)(y - 1.2) * 16 * worldScale) + WorldOrigin(worldIndex);
                        GameWorld.AddGameObject(new Portal(7, 4, content, "Portal", pos, worldScale), 1);
                    }
                    else if (_worlds[_worldIndex].world[y, x] > 90)          //secret portal, port-exit is second cipher

                    {
                        int exitRemainder = _worlds[_worldIndex].world[y, x] % 90;
                        Vector2 exit = GetSecretExit(worldScale, exitRemainder);
                        Vector2 pos = new Vector2(x * 16 * worldScale, (float)(y - 1.2) * 16 * worldScale) + WorldOrigin(worldIndex);
                        GameWorld.AddGameObject(new PortalSecret(7, 4, content, "secretPortal", pos, worldScale, exit), 1);
                    }
                    else if (_worlds[_worldIndex].world[y, x] == 75)                         //fade in portal,   second cipher = seconds;

                    {
                        int secs =Math.Abs(_worlds[_worldIndex].world[y, x] % 70);
                        
                        Vector2 pos = new Vector2(x * 16 * worldScale, (float)(y - 1.2) * 16 * worldScale) + WorldOrigin(worldIndex);
                        GameWorld.AddGameObject(new Portal(7, 4, content, "Portal", pos, worldScale, secs), 1);
                    }
                    else if (_worlds[_worldIndex].world[y, x] == -7)          //score display bg

                    {
                        Vector2 pos = new Vector2(x * 16 * worldScale, y * 16 * worldScale) + WorldOrigin(worldIndex);
                        GameWorld.AddGameObject(new Brick(0, 0, worldScale * 4, 1, content, "scoreBg", pos), 3);
                        GameWorld.ShowScore = true;
                        GameWorld.ScorePos = pos + new Vector2(5 * worldScale, 5 * worldScale);

                    }

                    else if (_worlds[_worldIndex].world[y, x] == -9)          //Cauldron

                    {
                        Vector2 pos = new Vector2(x * 16 * worldScale, y * 16 * worldScale + 4 * worldScale) + WorldOrigin(worldIndex);
                        GameWorld.AddGameObject(new AnimatedGameObject(1, 0, content, "cauldron", pos, worldScale), 0);
                    }
                    else if (_worlds[_worldIndex].world[y, x] == -8)          //witch

                    {
                        Vector2 pos = new Vector2(x * 16 * worldScale, y * 16 * worldScale - 5 * worldScale) + WorldOrigin(worldIndex);
                        GameWorld.AddGameObject(new Witch(1, 0, content, "witch", pos, worldScale), 1);
                    }
                    else if (_worlds[_worldIndex].world[y, x] == 17)          //heart

                    {
                        Vector2 pos = new Vector2(x * 16 * worldScale, y * 16 * worldScale - 30) + WorldOrigin(worldIndex);
                        GameWorld.AddGameObject(new Organ(content, "heart", pos, worldScale, OrganType.heart), 1);

                    }
                    else if (_worlds[_worldIndex].world[y, x] == 18)          //eye

                    {
                        Vector2 pos = new Vector2(x * 16 * worldScale, y * 16 * worldScale - 30) + WorldOrigin(worldIndex);
                        GameWorld.AddGameObject(new Organ(content, "eye", pos, worldScale, OrganType.eye), 1);

                    }
                    else if (_worlds[_worldIndex].world[y, x] == 19)          //brain

                    {
                        Vector2 pos = new Vector2(x * 16 * worldScale, y * 16 * worldScale - 30) + WorldOrigin(worldIndex);
                        GameWorld.AddGameObject(new Organ(content, "brain", pos, worldScale, OrganType.brain), 1);

                    }
                    else if (_worlds[_worldIndex].world[y, x] == -6)          //display string

                    {
                        Vector2 pos = new Vector2(x * 16 * worldScale, y * 16 * worldScale) + WorldOrigin(worldIndex);
                        Vector2 dispos = pos;
                        dispos.Y -= 160;                                            
                        //hack
                        if (_worldIndex ==2 || _worldIndex ==6)
                            dispos.Y -= 300;                                            

                        GameWorld.AddGameObject(new DisplayString(7, 0, worldScale*4, 8, DisplayString(), dispos, content, "bricks2", pos), 1);

                    }

                }
            }
        }

        private static void AddGrass(int x, int y, int worldScale, ContentManager content, Vector2 pos)
        {
            //brickgrass:
            if (y > 1 && _worlds[_worldIndex].world[y - 1, x] != 88) //x and y logic is flipped
            {
                _id++;
                Random rnd = new Random(_id);
                _typeIndexX = 0;
                _typeIndexY = rnd.Next(0, 8);

                GameWorld.AddGameObject(new Grass(_typeIndexX, _typeIndexY, _id, worldScale, 8, content, "grass2", pos), 3);

                //topgrass:
                //top right brick
                if (x + 1 < _worlds[_worldIndex].world.GetLength(1) && _worlds[_worldIndex].world[y - 1, x + 1] == 88)
                {
                    _typeIndexX = 1;
                    _typeIndexY = rnd.Next(0, 1);
                }
                //top left
                else if (x > 1 && _worlds[_worldIndex].world[y - 1, x - 1] == 88)
                {
                    _typeIndexX = 2;
                    _typeIndexY = rnd.Next(0, 1);
                }
                //standard
                else
                {
                    _typeIndexX = 1;
                    _typeIndexY = rnd.Next(2, 7);
                }
                pos.Y -= 16 * worldScale;
                GameWorld.AddGameObject(new Grass(_typeIndexX, _typeIndexY, _id, worldScale, 8, content, "grass2", pos), 0);
            }
        }
    }
}

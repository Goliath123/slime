﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlimeGame
{
    /// <summary>
    /// AnimatedGameObject is mainly used for animated sprites rendering
    /// </summary>
    class AnimatedGameObject : GameObject
    {
        private float _animationFPS = 0;
        private double _timeElapsed = 0;
        private int _currentAnimIndex = 0;
        protected Rectangle[] _animationRectangles;
        private int _frameCount;
        protected Color _spriteColor;

        /// <summary>
        /// Constructor for animated sprites
        /// </summary>
        /// <param name="frameCount"></param> how many images in the spritesheet
        /// <param name="animFPS"></param> anim speed
        /// <param name="content"></param> 
        /// <param name="spriteName"></param>
        /// <param name="pos"></param> position for obj to be rendered
        /// <param name="worldScale"></param> scale of world affects render size
        public AnimatedGameObject(int frameCount, float animFPS, ContentManager content, string spriteName, Vector2 pos, int worldScale) : base(content, spriteName, pos, worldScale)
        {
            _animationFPS = animFPS;
            _animationRectangles = new Rectangle[frameCount];
            for (int i = 0; i < frameCount; i++)
                _animationRectangles[i] = new Rectangle(i * _sprite.Width / frameCount, 0, _sprite.Width / frameCount, _sprite.Height);

            _currentAnimIndex = 0;
            _frameCount = frameCount;
            _spriteColor = new Color(255, 255, 255, 255);
        }

        public override Rectangle CollisionBox
        {
            get
            {
                return new Rectangle((int)_position.X, (int)_position.Y, _animationRectangles[0].Width * _scaleFactor, _animationRectangles[0].Height * _scaleFactor);
            }
        }

        public override void Update(GameTime gametime)
        {
            base.Update(gametime);
            _timeElapsed += gametime.ElapsedGameTime.TotalSeconds;
            _currentAnimIndex = (int)(_timeElapsed * _animationFPS);

            if (_currentAnimIndex > _animationRectangles.Count() - 1)
            {
                _currentAnimIndex = 0;
                _timeElapsed = 0;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_sprite, _position - GameWorld.Camera, _animationRectangles[_currentAnimIndex], _spriteColor, 0f, new Vector2(0, 0), _scaleVector, SpriteEffects.None, 0f);
        }



        //public override void DrawCollisionBox(SpriteBatch spriteBatch, Texture2D colTex)
        //{
        //    Rectangle cBox = CollisionBox;
        //    Rectangle top = new Rectangle(cBox.X, cBox.Y, _animationRectangles[0].Width * _scaleFactor, 1);
        //    Rectangle bottom = new Rectangle(cBox.X, cBox.Y + cBox.Height * _scaleFactor, _animationRectangles[0].Width * _scaleFactor, 1);
        //    Rectangle right = new Rectangle(cBox.X + _animationRectangles[0].Width * _scaleFactor, cBox.Y, 1, cBox.Height * _scaleFactor);
        //    Rectangle left = new Rectangle(cBox.X, cBox.Y, 1, cBox.Height * _scaleFactor);

        //   spriteBatch.Draw(colTex, top, Color.Red);
        //    spriteBatch.Draw(colTex, bottom, Color.Red);
        //    spriteBatch.Draw(colTex, right, Color.Red);
        //    spriteBatch.Draw(colTex, left, Color.Red);
        //}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace SlimeGame
{
    /// <summary>
    /// class to manage an organ being thrown by Witch, in the last level
    /// </summary>
    class OrganThrown : Organ
    {
        Vector2 _direction;
        Vector2 _endPos;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        /// <param name="spriteName"></param>
        /// <param name="pos"></param> start position of throw
        /// <param name="worldScale"></param>
        /// <param name="organ"></param> what organ it is
        /// <param name="endPos"></param> end position of throw
        public OrganThrown(ContentManager content, string spriteName, Vector2 pos, int worldScale, OrganType organ, Vector2 endPos) : base(content, spriteName, pos, worldScale, organ)
        {
            _direction = endPos-pos;
            _direction.Normalize();

            _endPos = endPos;

        }

        public override void Update(GameTime gametime)
        {
            _position += _direction * (float)gametime.ElapsedGameTime.TotalSeconds * 350;
            if ((_position.X - 130) < _endPos.X)
            {
                GameWorld.AddPoints(500);
                GameWorld.RemoveGameObject(this, 1);
            }
        }
    }
}

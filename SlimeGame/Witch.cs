﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace SlimeGame
{
    /// <summary>
    /// floating witch character, throws organs in last level 
    /// </summary>
    class Witch : GameObject
    {
        float _i = 0;
        float _organCounter = 0f;
        int _scale;

        bool _addedHeart;
        bool _addedEye;
        bool _addedBrain;

        int _levelIndex;

        ContentManager _content;
        public Witch(int frameCount, float animFPS, ContentManager content, string spriteName, Vector2 pos, int worldScale) : base(content, spriteName, pos, worldScale)
        {
            _scale = worldScale;
            _content = content;
            _levelIndex = Worlds.LevelIndex();
        }
        public override void Update(GameTime gametime)
        {
            _i += .05f;
            _position.Y += (_scale* (float)Math.Sin((double)_i) / 4);
            base.Update(gametime);

            if (_levelIndex == 6)

            {
                _organCounter += .2f;


                if (_organCounter > 4 && !_addedEye)
                {
                    _addedEye = true;
                    Vector2 endpos = Worlds.GetCauldronPos(_scale, Worlds.LevelIndex());
                    GameWorld.AddGameObject(new OrganThrown(_content, "eye", _position, _scale / 2, OrganType.eye, endpos), 1);
                }
                if (_organCounter > 8 && !_addedBrain)
                {
                    _addedBrain = true;
                    Vector2 endpos = Worlds.GetCauldronPos(_scale, Worlds.LevelIndex());
                    GameWorld.AddGameObject(new OrganThrown(_content, "brain", _position, _scale / 2, OrganType.brain, endpos), 1);
                }
                if (_organCounter > 11 && !_addedHeart)
                {
                    _addedHeart = true;
                    Vector2 endpos = Worlds.GetCauldronPos(_scale, Worlds.LevelIndex());
                    GameWorld.AddGameObject(new OrganThrown(_content, "heart", _position, _scale / 2, OrganType.heart, endpos), 1);
                }
            }
               
        }
    }


    
}

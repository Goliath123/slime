﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SlimeGame
{
    public class Slime : GameObject

    {
        int _startSpeed = 235; 
        int _gravityIncrease = 9;
        int _gravityMax = 250;
        private float _leftRightMoveCorr = .55f;


        private bool _speedStartToggle = false; //badName
        private bool _smoothFall = false; // badName
        private bool _falling = true;
        private bool _landToggle = true;

        private Vector2 _startPos = new Vector2(1, 1);

        private float _animationFPS = 0;
        private double _timeElapsed = 0;
        private int _currentAnimIndex = 0;
        private bool _playOnce = false;
        private Rectangle[] _animationRectangles;
        private Rectangle[] _idleRectangles;
        private Rectangle[] _leftRectangles;
        private Rectangle[] _rightRectangles;
        private Rectangle[] _jumpRectangles;
        private Rectangle[] _fallRectangles;
        private Rectangle[] _landRectangles;

        private int _frameCount;
        List<GameObject> _collisionList;
        List<GameObject> _collisionStair;

        private bool _cd = false;
        private bool _cu = false;
        private bool _cl = false;
        private bool _cr = false;


        private bool _gravity = true;
        private int _gravityForce = 0;
        private bool _isJumping = false;

        Rectangle _collisionTop;
        Rectangle _collisionBottom;
        Rectangle _collisionRight;
        Rectangle _collisionLeft;

        GameObject _tempCollisionObj;
        GameObject _tempCollisionObjLeft;
        GameObject _tempCollisionObjRight;
        GameObject _tempCollisionObjUp;

        private bool _canExitLevel = false;
        private bool _collidesStair = false;
        private bool _collideSecret = false;

        private SoundEffect _soundJump1;
        private SoundEffect _soundJump2;
        private SoundEffect _soundJump3;
        private SoundEffect _soundLand1;
        private SoundEffect _soundLand2;
        private SoundEffect _soundLand3;
        private SoundEffect _soundLand4;
        private Random _rnd = new Random();

        public bool IsJumping => _isJumping;
        public bool Gravity => _gravity;
        public bool LandToggle => _landToggle;
        public bool IsFalling => _falling;

        private bool _changeWorldSwitch = false;
        private ContentManager _content;

        private Vector2 _secretExit;

       // public bool CanPortToSecret { get; set; } = false;
        public bool CanExitLevel {get{ return _canExitLevel; }set { _canExitLevel = value; } }
        public bool CollidesStair { get => _collidesStair; set => _collidesStair = value; }



        public Slime(List<List<GameObject>> allGameObjectsList, int frameCount, float animFPS, ContentManager content, string spriteName, Vector2 pos, int worldScale) : base(content, spriteName, pos, worldScale)
        {
            _gravityIncrease *= _scaleFactor;
            _startSpeed *= _scaleFactor;
            _gravityMax *= _scaleFactor;
            _content = content;
            _collisionList = allGameObjectsList[2];
            _collisionStair = allGameObjectsList[1];

            _startPos = pos;

            _soundJump1 = content.Load<SoundEffect>("jump1");
            _soundJump2 = content.Load<SoundEffect>("jump2");
            _soundJump3 = content.Load<SoundEffect>("jump3");
            _soundLand1 = content.Load<SoundEffect>("land1");
            _soundLand2 = content.Load<SoundEffect>("land2");
            _soundLand3 = content.Load<SoundEffect>("land3");
            _soundLand4 = content.Load<SoundEffect>("land4");


            _animationFPS = animFPS;
            _animationRectangles = new Rectangle[frameCount];
            _idleRectangles = new Rectangle[frameCount];
            _leftRectangles = new Rectangle[frameCount];
            _rightRectangles = new Rectangle[frameCount];
            _jumpRectangles = new Rectangle[frameCount];
            _fallRectangles = new Rectangle[frameCount];
            _landRectangles = new Rectangle[frameCount];
            for (int i = 0; i < frameCount; i++)
            {
                _animationRectangles[i] = new Rectangle(i * _sprite.Width / frameCount, 0, _sprite.Width / frameCount, _sprite.Height / 6);
                _idleRectangles[i] = new Rectangle(i * _sprite.Width / frameCount, 0, _sprite.Width / frameCount, _sprite.Height / 6);
                _leftRectangles[i] = new Rectangle(i * _sprite.Width / frameCount, 1 * _sprite.Height / 6, _sprite.Width / frameCount, _sprite.Height / 6);
                _rightRectangles[i] = new Rectangle(i * _sprite.Width / frameCount, 2 * _sprite.Height / 6, _sprite.Width / frameCount, _sprite.Height / 6);
                _jumpRectangles[i] = new Rectangle(i * _sprite.Width / frameCount, 3 * _sprite.Height / 6, _sprite.Width / frameCount, _sprite.Height / 6);
                _fallRectangles[i] = new Rectangle(i * _sprite.Width / frameCount, 4 * _sprite.Height / 6, _sprite.Width / frameCount, _sprite.Height / 6);
                _landRectangles[i] = new Rectangle(i * _sprite.Width / frameCount, 5 * _sprite.Height / 6, _sprite.Width / frameCount, _sprite.Height / 6);

            }

            _currentAnimIndex = 0;
            _frameCount = frameCount;

        }

        public void SetSecretExit(Vector2 exit)
        {
            _secretExit = exit;
        }

        private void PlaySoundLand()
        {
            int i =_rnd.Next(1, 4);
            if (i == 1)
                _soundLand1.Play();
            else if (i == 2)
                _soundLand2.Play();
            else if (i == 3)
                _soundLand3.Play();
            else
                _soundLand4.Play();

        }
        private void PlaySoundJump()
        {
            int i = _rnd.Next(1, 3);
            if (i == 1)
                _soundJump1.Play();
            else if (i == 2)
                _soundJump2.Play();
            else
                _soundJump3.Play();

        }

        public override Rectangle CollisionBox
        {
            get
            {
                return new Rectangle((int)_position.X, (int)_position.Y, _animationRectangles[0].Width * _scaleFactor , _animationRectangles[0].Height * _scaleFactor);
            }
        }


        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_sprite, _position - GameWorld.Camera, _animationRectangles[_currentAnimIndex], Color.White, 0f, new Vector2(0, 0), _scaleVector, SpriteEffects.None, 0f);
            //spriteBatch.Draw(_sprite, _position, _animationRectangles[_currentAnimIndex], Color.White);
        }

        public bool IsCollidingDown(GameObject otherObj)
        {
            return _collisionBottom.Intersects(otherObj.CollisionBox);
        }

        public bool IsCollidingUp(GameObject otherObj)
        {
            return _collisionTop.Intersects(otherObj.CollisionBox);
        }

        public bool IsCollidingLeft(GameObject otherObj)
        {
            return _collisionLeft.Intersects(otherObj.CollisionBox);
        }

        public bool IsCollidingRight(GameObject otherObj)
        {
            return _collisionRight.Intersects(otherObj.CollisionBox);
        }

        public override void DrawCollisionBox(SpriteBatch spriteBatch, Texture2D collisionTex)
        {

            Rectangle cBox = CollisionBox;
            Rectangle Top = new Rectangle(cBox.X, cBox.Y, cBox.Width, 1);
            Rectangle Bottom = new Rectangle(cBox.X, cBox.Y + cBox.Height, cBox.Width, 1);
            Rectangle Right = new Rectangle(cBox.X + cBox.Width, cBox.Y, 1, cBox.Height);
            Rectangle Left = new Rectangle(cBox.X, cBox.Y, 1, cBox.Height);

            spriteBatch.Draw(collisionTex, _collisionTop, Color.Blue);
            spriteBatch.Draw(collisionTex, _collisionBottom, Color.Yellow);
            spriteBatch.Draw(collisionTex, _collisionRight, Color.Green);
            spriteBatch.Draw(collisionTex, _collisionLeft, Color.Black);

            //spriteBatch.Draw(collisionTex, Top, Color.Red);
            //spriteBatch.Draw(collisionTex, Bottom, Color.Red);
            //spriteBatch.Draw(collisionTex, Right, Color.Red);
            //spriteBatch.Draw(collisionTex, Left, Color.Red);

        }


        private void CheckCollisonLeft()
        {
            _cl = false;
            Rectangle cBox = CollisionBox;
            _collisionLeft = new Rectangle(cBox.X + 2*_scaleFactor, cBox.Y + 2*_scaleFactor, 1, cBox.Height - 4*_scaleFactor);

            foreach (GameObject obj in _collisionList)
            {
                if (IsCollidingLeft(obj))
                {
                    _tempCollisionObjLeft = obj;
                    _cl = true;
                }
            }
        }

        private void CheckCollisonRight()
        {
            _cr = false;
            Rectangle cBox = CollisionBox;
            _collisionRight = new Rectangle(cBox.X - 2 * _scaleFactor + cBox.Width, cBox.Y + 2 * _scaleFactor, 1, cBox.Height - 4 * _scaleFactor);

            foreach (GameObject obj in _collisionList)
            {
                if (IsCollidingRight(obj))
                {
                    _tempCollisionObjRight = obj;
                    _cr = true;
                }
            }
        }

        private void CheckCollisonUp()
        {
            _cu = false;
            Rectangle cBox = CollisionBox;
            _collisionTop = new Rectangle(cBox.X + 5 * _scaleFactor, cBox.Y + 2 * _scaleFactor, cBox.Width - 9 * _scaleFactor, 1);

            foreach (GameObject obj in _collisionList)
            {
                if (IsCollidingUp(obj))
                {
                    _cu = true;
                    _falling = true;
                    _gravityForce = 300; //to simulate jump stop 
                    _tempCollisionObjUp = obj;
                }
            }
        }
        private void CheckCollisonDown()
        {
            _cd = false;
            Rectangle cBox = CollisionBox;
            _collisionBottom = new Rectangle(cBox.X + 5 * _scaleFactor, cBox.Y  + cBox.Height, cBox.Width - 9 * _scaleFactor, 1);

            foreach (GameObject obj in _collisionList)
            {
                if (IsCollidingDown(obj))
                {
                    _cd = true;
                    _gravity = false;
                    _gravityForce = 0;
                    _falling = false;
                    _isJumping = false;
                    _tempCollisionObj = obj;
                }
            }
        }
        private void CheckCollisonStair()
        {
            _collidesStair = false;
            foreach (GameObject obj in _collisionStair)
            {
                if (obj is Stair && IsColliding(obj))
                {
                    _collidesStair = true;
                    _gravityForce = 300;
                }
            }
        }
        private void CheckCollisonSecret()
        {
            _collideSecret = false;
            foreach (GameObject obj in _collisionStair)
            {
                if (obj is PortalSecret && IsColliding(obj))
                    _collideSecret = true;
            }
        }

        private void CheckCollisons()
        {
            CheckCollisonStair();
            CheckCollisonSecret();

            CheckCollisonUp();
            CheckCollisonRight();
            CheckCollisonLeft();
            CheckCollisonDown();

        }

        private void PlaySlimeAnim(Rectangle[] _anim, bool playOnce=false)
        {
            _playOnce = playOnce;
           

            if (_anim[0] != _animationRectangles[0]) //new anim
            {
                _currentAnimIndex = 0;
                _timeElapsed = 0;
                _anim.CopyTo(_animationRectangles, 0);
            }




        }

        public override void Update(GameTime gametime)
        {

            CheckCollisons();

            _currentAnimIndex = (int)(_timeElapsed * _animationFPS);
            _timeElapsed += gametime.ElapsedGameTime.TotalSeconds;
            if (_currentAnimIndex > _animationRectangles.Count() - 1 && !_playOnce)
            {
                _currentAnimIndex = 0;
                _timeElapsed = 0;
                PlaySlimeAnim(_idleRectangles, false);


            }
            else if (_currentAnimIndex > _animationRectangles.Count() - 1 && _playOnce)
            {
                _currentAnimIndex = 0;
                _timeElapsed = 0;
                PlaySlimeAnim(_idleRectangles, false);
            }


            if (Keyboard.GetState().IsKeyDown(Keys.Left) && !_cl)                                //LEFT
            {
                _position.X -= _startSpeed * _leftRightMoveCorr * (float)gametime.ElapsedGameTime.TotalSeconds; 
                PlaySlimeAnim(_leftRectangles, false); //spil "venstre" animation

                CheckCollisonLeft();    //collider jeg med noget til venstre
                if (_cl)
                    CorrectLeft();      //hvis ja, koriger position for at undgå intersection

                if (!_cd && !_collidesStair)
                    _gravity = true;
            }
            

                if (Keyboard.GetState().IsKeyDown(Keys.Right) && !_cr)                          //RIGHT
            {
                _position.X += _startSpeed * _leftRightMoveCorr * (float)gametime.ElapsedGameTime.TotalSeconds;
                PlaySlimeAnim(_rightRectangles, false);

                CheckCollisonRight();
                if (_cr)
                    CorrectRight();

                if (!_cd && !_collidesStair)
                    _gravity = true;

            }

            if (Keyboard.GetState().IsKeyDown(Keys.Up) && !_cu)                                 //JUMP
            {

                if (_collidesStair)
                {
                    _position.Y -= _startSpeed * _leftRightMoveCorr * (float)gametime.ElapsedGameTime.TotalSeconds;
                    //_gravity = false; _gravityForce = 0;
                }
                else
                {
                    if (_gravityForce < _startSpeed && !_falling && !_isJumping)
                    {
                        _position.Y -= _startSpeed * (float)gametime.ElapsedGameTime.TotalSeconds;
                        _smoothFall = true;
                        if (!_landToggle)
                        {
                            PlaySlimeAnim(_jumpRectangles, false);
                            _landToggle = true;
                            PlaySoundLand();
                        }
                        CheckCollisonUp();
                        if (_cu)
                            CorrectUp();
                    }
                    else
                    {
                        //one time reduction in gravForce
                        if (_smoothFall)
                        {
                            _isJumping = false;
                            _smoothFall = false;
                            _gravityForce -= (int)(_startSpeed * 0.8);
                            _falling = true;
                            PlaySlimeAnim(_fallRectangles, false);
                        }
                    }
                   // _isJumping = true;


                    if (!_collidesStair)
                        _gravity = true;

                    _speedStartToggle = true;
                }
                
            }
            if (Keyboard.GetState().IsKeyUp(Keys.Up) && !_collidesStair)       //isjumping set on key release 
            {
                //if(!_collidesStair)
                //_isJumping = true;


                if (_speedStartToggle && !_falling)
                {
                    _speedStartToggle = false;
                    _gravityForce -= (int)(_startSpeed * 0.5);
                }

                if (!_gravity)
                {
                    if (_landToggle )
                    {
                        _landToggle = false;
                        PlaySlimeAnim(_landRectangles, true);
                        CorrectPositionDown();                      //TODO: gets called on key down..zzz
                        PlaySoundLand();
                    }

                }
                 else if (!_falling)
                {
                    _falling = true;
                    _landToggle = true;
                    PlaySlimeAnim(_fallRectangles, false);

                }

                //TODO: consider if perma jump is ok
                //_newJump = true;            //release jump key to jump again


            }
            if (Keyboard.GetState().IsKeyDown(Keys.Down) )                                          //down
            {
                if (_collidesStair && !_cd)
                {
                    _position.Y += _startSpeed * _leftRightMoveCorr * (float)gametime.ElapsedGameTime.TotalSeconds;
                    _gravity = false; _gravityForce = 0;
                }

                if ( _canExitLevel && _changeWorldSwitch)
                {
                    int nextWorld = Worlds.NextLevelIndex();
                    int nextScale = Worlds.NextLevelScale(nextWorld);
                    GameWorld.ChangeWorld(nextWorld, nextScale, _content);
                    _changeWorldSwitch = false;
                }

                else if (_collideSecret)
                {
                    _position = _secretExit;
                    _collideSecret = false;
                    Console.WriteLine("exiting secret");
                    _gravity = true;
                }
            }
             
            if (Keyboard.GetState().IsKeyDown(Keys.Down) && _canExitLevel && _changeWorldSwitch)
            {
                
            }
            if (Keyboard.GetState().IsKeyUp(Keys.Down))
            {
                _changeWorldSwitch = true;
            }


            if (_gravityForce > _gravityMax)
                 _gravityForce = _gravityMax;


            if (_gravity && !_collidesStair)
            {

                _gravityForce += _gravityIncrease;
                int i = (int)(_gravityForce * (float)gametime.ElapsedGameTime.TotalSeconds);
                //Console.WriteLine(i);
                _position.Y += i;

            }

            //if(IsJumping)
            //     CorrectPositionDown();

 
            if (!GameWorld.ScreenSize.Intersects(this.CollisionBox))
            {
                _position = _startPos;
            }

            GameWorld.MoveCamera(_position, gametime);

            base.Update(gametime);
        }

        private void CorrectPositionDown()
        {
                _position.Y = _tempCollisionObj.CollisionBox.Y - 13 * _scaleFactor; 
        }
        private void CorrectLeft()
        {
                _position.X = _tempCollisionObjLeft.CollisionBox.X + 14 * _scaleFactor;
        }

        private void CorrectRight()
        {
                _position.X = _tempCollisionObjRight.CollisionBox.X - 14 * _scaleFactor;
        }
        private void CorrectUp()
        {
            _position.Y = _tempCollisionObjUp.CollisionBox.Y + 13 * _scaleFactor;
        }
    }
}

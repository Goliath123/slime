﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Threading;

namespace SlimeGame
{
    /// <summary>
    /// This is the main type for the game.
    /// </summary>
    public class GameWorld : Game
    {
       // GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private SpriteFont _exampleFont;
        private SpriteFont _scoreFont;
        private Texture2D _collisionTex;

        /// <summary>
        /// camera is a vector used to offset most rendered gameobjects to simulate a camera
        /// </summary>
        public static Vector2 Camera { get; set; } = new Vector2(0, 0);
        private static Vector2 _cameraTemp = new Vector2(0, 0);
        private static int _cameraStartMaxX = 1300;
        private static int _cameraStartMinX = 300;
        private static int _cameraMaxX = 1300;
        private static int _cameraMinX = 300;
        private static float _tempX = 0;      //badName
        private static float _tempY = 0;
        private static int _cameraMaxY = 650;
        private static int _cameraMinY = 250;
        private static int _cameraStartMaxY = 650;
        private static int _cameraStartMinY = 250;


        private static List<List<GameObject>> _toBeRemoved = new List<List<GameObject>>() {new List<GameObject>(), new List<GameObject>(), new List<GameObject>(), new List<GameObject>() };
        private static List<List<GameObject>> _toBeAdded = new List<List<GameObject>>() { new List<GameObject>(), new List<GameObject>(), new List<GameObject>(), new List<GameObject>() };
        private static Slime _slime;

       // private static int _listIndex = 0;
        private static List<List<GameObject>> _allGameObjectsList = new List<List<GameObject>>() { new List<GameObject>(), new List<GameObject>(), new List<GameObject>(), new List<GameObject>() };

        private static GraphicsDeviceManager graphics;

        private static int _points = 0;
        public static Vector2 ScorePos { get; set; }
        public static bool ShowScore { get; set; } = true;

        public static bool FoundHeart { get; set; } = false;
        public static bool FoundEye { get; set; } = false;
        public static bool FoundBrain { get; set; } = false;

        public static Slime Slime { get => _slime; set => _slime = value; }

       // public ContentManager ContentM { get { return Content; } }

            /// <summary>
            /// score property
            /// </summary>
            /// <param name="points"></param> points to be added
        public static void AddPoints(int points)
        {
            _points += points;
        }

            /// <summary>
            /// Adds a GameObject after standard updatelogic
            /// </summary>
            /// <param name="obj"></param>gameObj to be added
            /// <param name="layer"></param> 0=bg, 1=colliders, 2=environment, 3=nonCollidingEnvironment
        public static void AddGameObject(GameObject obj, int layer)
        {
            _toBeAdded[layer].Add(obj);
        }

        /// <summary>
        /// removes a GameObject after standard updatelogic
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="layer"></param> 0=bg, 1=colliders, 2=environment, 3=nonCollidingEnvironment
        public static void RemoveGameObject(GameObject obj, int layer)
        {
            _toBeRemoved[layer].Add(obj);
        }

        /// <summary>
        /// removes all gameobjects, then adds sky, world, slime and UI
        /// </summary>
        /// <param name="worldIndex"></param> what world to show
        /// <param name="worldScale"></param>scale of world
        /// <param name="content"></param>
        public static void ChangeWorld(int worldIndex, int worldScale, ContentManager content)
        {
            //_worldScale = worldScale;
            if (!Worlds.initialized)
                Worlds.Initialize();


            RemoveAllGameObjects();

            int nextWorld = Worlds.NextLevelIndex();
            
            int nextScale = Worlds.NextLevelScale(nextWorld);
            string nextBg = Worlds.NextLevelBg();

            AddGameObject(new Sky(content, nextBg, new Vector2(-100, -200), nextScale), 0);
            
            Worlds.BuildWorld(content, nextWorld, nextScale);

            if (nextWorld >1)
                AddUIElements(content);

           Camera = Worlds.LevelCamOrigin(); 

            Vector2 spawn = Worlds.GetSpawnPoint(worldScale, nextWorld);
            _slime = new Slime(_allGameObjectsList, 5, 15, content, "Slime", spawn, nextScale);
            AddGameObject(_slime, 1);

        }

       
        /// <summary>
        /// adds a box with room for 3 collecable organs, heart eye and brain - at the top of the screen
        /// </summary>
        /// <param name="content"></param>
        private static void AddUIElements(ContentManager content)
        {
            AddGameObject(new UI(0, 0, 8,content, "scoreBg", new Vector2(680, 10), 5, Color.White), 3);

            AddGameObject(new OrganUI(content, "heart", new Vector2(720, 20), 2, OrganType.heart), 3);
            AddGameObject(new OrganUI(content, "eye", new Vector2(780, 30), 2, OrganType.eye), 3);
            AddGameObject(new OrganUI(content, "brain", new Vector2(840, 20), 2, OrganType.brain), 3);
        }

        /// <summary>
        /// removes all gameobjects in _allGameObjectsList 
        /// </summary>
        private static  void RemoveAllGameObjects()
        {
            for (int i = 0; i < _allGameObjectsList.Count; i++)        
                foreach (GameObject obj in _allGameObjectsList[i])
                    _toBeRemoved[i].Add(obj);
        }

        /// <summary>
        /// returns a vector at the center of the screen
        /// </summary>
        /// <returns></returns>
        public static Vector2 ScreenCenter()
        {
            return new Vector2(graphics.PreferredBackBufferWidth / 2, graphics.PreferredBackBufferHeight / 2);
        }


        /// <summary>
        /// constructor sets screensize and gfxdevice manager, and content.rootdirectory
        /// </summary>
        public GameWorld()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1600;
            graphics.PreferredBackBufferHeight = 900;

            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// returns the actual renderspace, modified by the Camera Vector
        /// </summary>
        public static Rectangle ScreenSize
        {
            get
            {
                int outsideboundsX = 200;
                int outsideboundsY = 100;

                Rectangle _rect = new Rectangle(graphics.GraphicsDevice.Viewport.X + (int)Camera.X- outsideboundsX, graphics.GraphicsDevice.Viewport.Y- outsideboundsY + (int)Camera.Y, graphics.GraphicsDevice.Viewport.Width+ outsideboundsX*2, graphics.GraphicsDevice.Viewport.Height + outsideboundsY * 2);
                return _rect;
            }
            
        }


        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
          
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            _exampleFont = Content.Load<SpriteFont>("ExampleFont");
            _scoreFont = Content.Load<SpriteFont>("smalle");

            _collisionTex = Content.Load<Texture2D>("CollisionTexture");

            ChangeWorld(0, 4, Content);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();


            foreach (GameObject go in _allGameObjectsList[1])
            {
                go.Update(gameTime);
                foreach (GameObject other in _allGameObjectsList[1])
                {
                    if (go != other && go.IsColliding(other))
                    {
                        go.DoCollision(other);
                    }
                }
            }

            for (int i = 0; i < _toBeRemoved.Count; i++)        //i serves as layer for rendering
                foreach (GameObject go in _toBeRemoved[i])
                    _allGameObjectsList[i].Remove(go);

            foreach (List<GameObject> list in _toBeRemoved)
                list.Clear();


            for (int i = 0; i < _toBeAdded.Count; i++)          //i serves as layer for rendering in draw
                foreach (GameObject go in _toBeAdded[i])
                    _allGameObjectsList[i].Add(go);


            foreach (List<GameObject> list in _toBeAdded)
                list.Clear();
           
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);


            //samplerstate.pointclamp makes scaled sprites keep their sharpness
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);

    
            foreach (List<GameObject> list in _allGameObjectsList)
                foreach (GameObject obj in list)
                    obj.Draw(spriteBatch);

            //foreach (GameObject obj in _allGameObjectsList[1])
            //    obj.DrawCollisionBox(spriteBatch, _collisionTex);

            //foreach (GameObject obj in _allGameObjectsList[2])
            //    obj.DrawCollisionBox(spriteBatch, _collisionTex);


           // spriteBatch.DrawString(_exampleFont, "jump " + $"{_slime.IsJumping}", new Vector2(140, 120), Color.Black);
          //  spriteBatch.DrawString(_exampleFont, "grav " + $"{_slime.Gravity}", new Vector2(140, 140), Color.Black);
          //  spriteBatch.DrawString(_exampleFont, "land Toggle " + $"{_slime.LandToggle}", new Vector2(140, 160), Color.Black);
            //spriteBatch.DrawString(_exampleFont, "falling " + $"{_slime.IsFalling}", new Vector2(140, 180), Color.Black);

            if(ShowScore)
                spriteBatch.DrawString(_scoreFont, "points " + $"{_points}", ScorePos-Camera, Color.PaleGreen , 0, Vector2.Zero, Worlds.LevelScale() * .4f, SpriteEffects.None, 0f);

            //spriteBatch.DrawString(_exampleFont, "stair " + $"{_slime.CollidesStair}", new Vector2(140, 250), Color.Black);
           // spriteBatch.DrawString(_exampleFont, "cam " + $"{Camera}", new Vector2(140, 270), Color.Black);


            spriteBatch.End();
            base.Draw(gameTime);
        }

        internal static void MoveCamera(Vector2 position, GameTime gametime)
        {
            if (position.X > _cameraMaxX)
            {
                _cameraMaxX = (int)position.X;
                _cameraMinX = _cameraMaxX - _cameraStartMaxX + _cameraStartMinX;
            }
            else if (position.X < _cameraMinX)
            {
                _cameraMinX = (int)position.X;
                _cameraMaxX = _cameraMinX + _cameraStartMaxX - _cameraStartMinX;
            }

            if (position.Y > _cameraMaxY)
            {
                _cameraMaxY = (int)position.Y;
                _cameraMinY = _cameraMaxY - _cameraStartMaxY + _cameraStartMinY; 
            }
            else if (position.Y < _cameraMinY)
            { 
                _cameraMinY = (int)position.Y;
                _cameraMaxY = _cameraMinY + _cameraStartMaxY - _cameraStartMinY;
            }



            if (((Camera.X + _cameraStartMinX)- _cameraMinX) < -20)
            {
                _tempX = Camera.X + 129.5f * Worlds.LevelScale() * (float)gametime.ElapsedGameTime.TotalSeconds; //TODO expose slimespeed and store in var
            }
            else if (((Camera.X + _cameraStartMinX) - _cameraMinX) > 20)
            {
                _tempX = Camera.X - 129.5f * Worlds.LevelScale() * (float)gametime.ElapsedGameTime.TotalSeconds;
            }



            if (((Camera.Y + _cameraStartMinY) - _cameraMinY) > 20)
            {
                _tempY = Camera.Y - 129.5f * Worlds.LevelScale() * (float)gametime.ElapsedGameTime.TotalSeconds; //TODO expose slimespeed and store in var
            }
            else if (((Camera.Y + _cameraStartMinY) - _cameraMinY) < -20)
            {
                _tempY = Camera.Y + 129.5f * Worlds.LevelScale() * (float)gametime.ElapsedGameTime.TotalSeconds;
            }

            _cameraTemp.X = _tempX;
            _cameraTemp.Y = _tempY;

            Camera = _cameraTemp;
        }
    }
}

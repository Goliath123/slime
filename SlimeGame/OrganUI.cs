﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace SlimeGame
{
    /// <summary>
    /// class to manage organs renderen on a UI square
    /// </summary>
    class OrganUI : UI
    {
        Color _color;
        OrganType _organ;
        public OrganUI(ContentManager content, string spriteName, Vector2 pos, int worldScale, OrganType organ) : base(content, spriteName, pos, worldScale)
        {
            _organ = organ;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (_organ == OrganType.brain && GameWorld.FoundBrain || _organ == OrganType.eye && GameWorld.FoundEye || _organ == OrganType.heart && GameWorld.FoundHeart)
                _color = Color.White;
            else
                _color = Color.Black;
            spriteBatch.Draw(_sprite, _position , _sprite.Bounds, _color, 0f, new Vector2(0, 0), _scaleVector, SpriteEffects.None, 0f);
        }

    }
}

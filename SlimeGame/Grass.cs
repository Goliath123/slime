﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace SlimeGame
{
    /// <summary>
    /// grass to be rendered on top of Bricks
    /// </summary>
    class Grass : GameObject
    {
        private Rectangle _rect;

        public Grass(int xidx, int yidx, int id, int worldScale, int frameCount, ContentManager content, string spriteName, Vector2 pos) : base(content, spriteName, pos, worldScale)
        {
            _scaleFactor = worldScale;
            _scaleVector = new Vector2((float)worldScale / 4, (float)worldScale / 4);

            _rect = new Rectangle(xidx * _sprite.Width / frameCount, yidx * _sprite.Width / frameCount, _sprite.Width / frameCount, _sprite.Height / frameCount);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_sprite, _position-GameWorld.Camera, _rect, Color.White, 0f, new Vector2(0, 0), _scaleVector, SpriteEffects.None, 0f);
        }
    }
}

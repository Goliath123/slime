﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;

namespace SlimeGame
{
    /// <summary>
    /// pickup Gem
    /// </summary>
    class Gem : AnimatedGameObject
    {
        private SoundEffect _soundGem;

        public Gem(int frameCount, float animFPS, ContentManager content, string spriteName, Vector2 pos, int worldScale) : base(frameCount, animFPS, content, spriteName, pos, worldScale)
        {
            _soundGem = content.Load<SoundEffect>("coin");
        }

        public override void DoCollision(GameObject otherObject)
        {
            if (otherObject is Slime) 
            {
                GameWorld.RemoveGameObject(this, 1);
                GameWorld.AddPoints(500);
                _soundGem.Play();
            }
        }
    }
}

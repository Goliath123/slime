﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace SlimeGame 
{
    /// <summary>
    /// background image of the sky rendered at center of the screen
    /// </summary>
    class Sky : GameObject
    {
        Vector2 _origin;
        Vector2 _screenCenter;
        public Sky(ContentManager content, string spriteName, Vector2 pos, int worldScale) : base(content, spriteName, pos, worldScale)
        {
            _origin = new Vector2(_sprite.Width / 2, _sprite.Height / 2);
            _screenCenter = GameWorld.ScreenCenter();
        }


        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_sprite, _screenCenter - GameWorld.Camera/5, CollisionBox, Color.White, 0f, _origin, _scaleVector, SpriteEffects.None, 0f);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace SlimeGame
{
    /// <summary>
    /// class to manage a portal, which is the transition between levels
    /// </summary>
    class Portal : AnimatedGameObject
    {
        float _fade = 0f;
        int _alpha =0;
        bool _shouldFade;

        /// <summary>
        /// constructor for the portal
        /// </summary>
        /// <param name="frameCount"></param>
        /// <param name="animFPS"></param>
        /// <param name="content"></param>
        /// <param name="spriteName"></param>
        /// <param name="pos"></param>
        /// <param name="worldScale"></param>
        /// <param name="fade"></param> if over 0, the portal is transparent untill "fade" is reached, and it fades in
        public Portal(int frameCount, float animFPS, ContentManager content, string spriteName, Vector2 pos, int worldScale, float fade=0) : base(frameCount, animFPS, content, spriteName, pos, worldScale)
        {
            _fade = fade;
            if (_fade > 1)
                _shouldFade = true;


        }

        public override Rectangle CollisionBox
        {
            get
            {
                int w = _animationRectangles[0].Width * _scaleFactor;
                return new Rectangle((int)_position.X + (int)(w * 0.2f), (int)_position.Y, w / 2, (int)((_animationRectangles[0].Height * _scaleFactor) *.9f));
            }
        }


        public override void Update(GameTime gametime)
         {

            base.Update(gametime);

            if (IsColliding(GameWorld.Slime))
            {
             GameWorld.Slime.CanExitLevel = true;
                if (!Worlds.IsShowingLitArrow)
                {
                    Worlds.ShowLitArrow();
                    Worlds.IsShowingLitArrow = true;
                }

            }
            else
            {
                GameWorld.Slime.CanExitLevel = false;

                if (Worlds.IsShowingLitArrow)
                {
                    Worlds.ShowUnlitArrow();
                    Worlds.IsShowingLitArrow = false;
                }
            }

            if (_shouldFade)
            {
                _fade -= (float)gametime.ElapsedGameTime.TotalSeconds * 2;
                if (_fade < 0)
                {
                    _alpha += 15;
                }
                MathHelper.Clamp(_alpha, 0, 255);
            }
            else
                _alpha = 255;

            base._spriteColor = new Color(_alpha, _alpha, _alpha, _alpha);


        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace SlimeGame
{
    /// <summary>
    ///  square that draws a string at a given position, on collision
    /// </summary>
    class DisplayString :GameObject
    {
        int _typeIndexX;
        int _typeIndexY;
        Rectangle _rect;
        private bool _display;
        Vector2 _dispos;
        SpriteFont _displayFont;
        string _displayString;
        Texture2D _ct;

        /// <summary>
        /// constructor for the class
        /// </summary>
        /// <param name="indexX"></param>used by spritesheet
        /// <param name="indexY"></param>used by spritesheet
        /// <param name="worldScale"></param>
        /// <param name="frameCount"></param> rows in sheet
        /// <param name="displayString"></param> string to be displayed
        /// <param name="displayPos"></param> position for displayed string
        /// <param name="content"></param>
        /// <param name="spriteName"></param> name of sheet
        /// <param name="pos"></param> position for collider
        public DisplayString(int indexX, int indexY, int worldScale, int frameCount,string displayString, Vector2 displayPos, ContentManager content, string spriteName, Vector2 pos) : base(content, spriteName, pos, worldScale)
        {
            _displayFont = content.Load<SpriteFont>("smalle");
            _ct = content.Load<Texture2D>("CollisionTexture");

            _displayString = displayString;
            _dispos = displayPos;
            _scaleVector = new Vector2((float)_scaleFactor / 4, (float)_scaleFactor / 4);
            _typeIndexX = indexX;
            _typeIndexY = indexY;
            _rect = new Rectangle(_typeIndexX * _sprite.Width / frameCount, _typeIndexY * _sprite.Width / frameCount, _sprite.Width / frameCount, _sprite.Height / frameCount);
        }


        public override void Update(GameTime gametime)
        {
            if (IsColliding(GameWorld.Slime))
                _display = true;
            else
                _display = false;
        }

        public override Rectangle CollisionBox
        {
            get
            {
                return new Rectangle((int)_position.X, (int)_position.Y, _rect.Width * _scaleFactor / 4, _rect.Height * _scaleFactor / 4);
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_sprite, _position - GameWorld.Camera, _rect, Color.White, 0f, new Vector2(0, 0), _scaleVector, SpriteEffects.None, 0f);

            if (_display)
                spriteBatch.DrawString(_displayFont, $"{_displayString}", _dispos - GameWorld.Camera, Color.DarkOliveGreen, 0, Vector2.Zero, Worlds.LevelScale() * .4f, SpriteEffects.None, 0f);
            
        }

        public DisplayString(ContentManager content, string spriteName, Vector2 pos, int worldScale) : base(content, spriteName, pos, worldScale)
        {
        }
    }
}

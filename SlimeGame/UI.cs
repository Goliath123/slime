﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace SlimeGame
{
    /// <summary>
    /// class to render UI objects unaffected by GameWorld.Camera
    /// </summary>
    class UI : GameObject
    {
        public UI(ContentManager content, string spriteName, Vector2 pos, int worldScale) : base(content, spriteName, pos, worldScale)
        {
        }
        //        public Brick(int indexX, int indexY, int worldScale, int frameCount, ContentManager content, string spriteName, Vector2 pos) : base(content, spriteName, pos, worldScale)
        public UI(int indexX, int indexY, int frameCount, ContentManager content, string spriteName, Vector2 pos, int worldScale, Color color) : base(content, spriteName, pos, worldScale)
        {
        }

        public override void Update(GameTime gametime)
        {
            base.Update(gametime);
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_sprite, _position, _sprite.Bounds, Color.White, 0f, new Vector2(0, 0), _scaleVector, SpriteEffects.None, 0f);
        }
    }
}

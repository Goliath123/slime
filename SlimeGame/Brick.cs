﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SlimeGame
{
    /// <summary>
    /// standard squared brick for worldbuilding
    /// </summary>
    class Brick : GameObject
    {
        protected Rectangle _rect;

        protected int _typeIndexX = 0;
        protected int _typeIndexY = 0;
        protected ContentManager _content; //TODO: learn how to access Contentmanager in non-static Gameworld when Gameworld instance is inacessible
        private int _frameCount = 0;

        /// <summary>
        /// constructor with random x and y index for random brick gfx;
        /// </summary>
        /// <param name="id"></param>incrementing int used by Random
        /// <param name="worldScale"></param>
        /// <param name="frameCount"></param>columns on spritesheet
        /// <param name="content"></param>
        /// <param name="spriteName"></param>
        /// <param name="pos"></param>
        public Brick(int id, int worldScale, int frameCount, ContentManager content, string spriteName, Vector2 pos) : base(content, spriteName, pos, worldScale)
        {
            _content = content;
            _frameCount = frameCount;
            _scaleVector = new Vector2((float)_scaleFactor / 4, (float)_scaleFactor / 4);

            Random rnd = new Random(id);
            if (rnd.Next(0, 100) < 50)              //brick sprite x-axis
                _typeIndexX = 0;
            else
                _typeIndexX = rnd.Next(1, 7);

            if (rnd.Next(0, 100) < 75)              //brick sprite y-axis
                _typeIndexY = rnd.Next(0, 3);
            else
                _typeIndexY = rnd.Next(4, 8);

            _rect = new Rectangle(_typeIndexX * _sprite.Width / frameCount, _typeIndexY * _sprite.Width / frameCount, _sprite.Width / frameCount, _sprite.Height / frameCount);
        }

        /// <summary>
        /// constructer with specified x and y to determine rectangle on spritesheet
        /// </summary>
        /// <param name="indexX"></param> x on spritesheet
        /// <param name="indexY"></param> y on spritesheet
        /// <param name="worldScale"></param>
        /// <param name="frameCount"></param> columns on spritesheet
        /// <param name="content"></param>
        /// <param name="spriteName"></param>
        /// <param name="pos"></param>
        public Brick(int indexX, int indexY, int worldScale, int frameCount, ContentManager content, string spriteName, Vector2 pos) : base(content, spriteName, pos, worldScale)
        {
            _content = content;
            _frameCount = frameCount;

            _scaleVector = new Vector2((float)_scaleFactor / 4, (float)_scaleFactor / 4);
            _typeIndexX = indexX;
            _typeIndexY = indexY;
            _rect = new Rectangle(_typeIndexX * _sprite.Width / frameCount, _typeIndexY * _sprite.Width / frameCount, _sprite.Width / frameCount, _sprite.Height / frameCount);
        }

       /// <summary>
       /// changes the image on the spritesheet 
       /// </summary>
       /// <param name="xPos"></param>sheet coord
       /// <param name="yPos"></param>sheet coord
        public void ChangeTexture(int xPos, int yPos)
        {
            _rect = new Rectangle(xPos * _sprite.Width / _frameCount, yPos * _sprite.Width / _frameCount, _sprite.Width / _frameCount, _sprite.Height / _frameCount);
        }


        public override Rectangle CollisionBox
        {
            get
            {
                return new Rectangle((int)_position.X, (int)_position.Y, _rect.Width * _scaleFactor / 4, _rect.Height * _scaleFactor / 4);
            }
        }


        public override string ToString()
        {
            return base.ToString();
        }


        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_sprite, _position-GameWorld.Camera, _rect, Color.White, 0f, new Vector2(0, 0), _scaleVector, SpriteEffects.None, 0f);
        }
    }
}

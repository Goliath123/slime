﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlimeGame
{
    /// <summary>
    /// struct to hold any level info
    /// </summary>
    public struct WorldInfo
    {
        public int[,] world;
        public Vector2 worldOrigin;
        public Vector2 camOrigin;
        public int worldScale;
        public string background;
        public string displayString;

        /// <summary>
        /// constructor of world infos
        /// </summary>
        /// <param name="world"></param> 2d int array with numbers each representing an object to be added/drawn, by Worlds class
        /// <param name="worldOrigin"></param>an offset to place the world correctly on screen
        /// <param name="camOrigin"></param>an offset to place Camera 
        /// <param name="worldScale"></param>scale of the world, 1-12 is intended
        /// <param name="bg"></param>name of background image world will use
        /// <param name="displayString"></param> if DisplayString class is present, it will display this string
        public WorldInfo(int[,] world, Vector2 worldOrigin, Vector2 camOrigin, int worldScale, string bg, string displayString ="")
        {
            this.world = world;
            this.worldOrigin = worldOrigin;
            this.camOrigin = camOrigin;
            this.worldScale = worldScale;
            this.background = bg;
            this.displayString = displayString;
        }
    }
}
